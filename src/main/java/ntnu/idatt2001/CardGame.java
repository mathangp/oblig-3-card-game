package ntnu.idatt2001;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import ntnu.idatt2001.cardgame.HandOfCards;
import ntnu.idatt2001.cardgame.PlayingCard;

import java.util.List;

public class CardGame extends Application {

    //textfields that show only information about the player's cards
    private TextField sumOfFaces = new TextField();
    private TextField cardsOfHearts = new TextField();
    private TextField queenOfSpades = new TextField();
    private TextField flushText = new TextField();

    private TextField input = new TextField(); //takes in input from the player on the number of cards to deal
    private TextField text = new TextField(); //shows the player the number of cards that have been dealt out

    //buttons that the user can click on
    private Button dealHand;
    private Button checkHand;
    private Button enter;

    //default starting number for dealing out cards
    private int numOfCards = 5;

    // Displays the player's cards in text form
    private Label label;

    // an empty list of cards
    private HandOfCards playerCards = new HandOfCards();

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {
        stage.setTitle("Card Game");

        BorderPane game = new BorderPane();
        game.setPadding(new Insets(50,50,50,50));

        text.setMaxWidth(200);
        text.setDisable(true);
        text.setText("Default No. of Cards: " + numOfCards);
        text.setStyle("-fx-font-weight: bold");

        game.setTop(text);
        game.setBottom(actionButtonsDisplay());
        game.setLeft(displayBox());
        game.setRight(informasjonDisplayBox());

        //the player cannot edit these textfields
        sumOfFaces.setEditable(false);
        cardsOfHearts.setEditable(false);
        queenOfSpades.setEditable(false);
        flushText.setEditable(false);

        sumOfFaces.setStyle("-fx-font-weight: bold");
        cardsOfHearts.setStyle("-fx-font-weight: bold");
        queenOfSpades.setStyle("-fx-font-weight: bold");
        flushText.setStyle("-fx-font-weight: bold");

        dealHandAction();
        checkHandAction();
        enterAction();

        Scene scene = new Scene(game, 800, 400);
        stage.setScene(scene);
        stage.show();
    }

    /**
     * When dealHand button is pressed, and if the number of cards is valid,
     * all the cards will be displayed in a grey box
     *
     * If the number of cards is invalid, the player will get a message in red about what went wrong
     */
    private void dealHandAction(){
        dealHand.setOnAction(actionEvent -> {
            try{
                playerCards = new HandOfCards(numOfCards);
                //Displays all the cards the player gets if numOfCards is valid
                label.setText(playerCards.toString());
                label.setTextFill(Color.BLACK);
                /*
                resets all previous information in the textfields whenever dealHand is clicked
                 */
                resetTextFields();
            }catch(IllegalArgumentException error){
                label.setText(error.getMessage());
                label.setTextFill(Color.RED);
            }

        });

    }

    /**
     * When checkHand button is pressed, and the dealHand button was pressed before this,
     * information about the cards are displayed on the right side of the application.
     * Information that is displayed: Sum of faces
     *                                Cards of hearts
     *                                Queen of Spades (Yes/No)
     *                                Flush (Yes/No)
     */
    private void checkHandAction(){
        checkHand.setOnAction(actionEvent -> {
            //if player click on check hand before deal hand, player will get asked to deal hand first.
            if (playerCards.getPlayerCards().isEmpty()){
                label.setText("You do not have any cards yet. \nDeal Hand first.");
                label.setTextFill(Color.RED);
            }else{
                //Displays "Yes" if the player got a flush, "No" otherwise
                if (playerCards.checkFor5CardsFlush() == true){
                    flushText.setText("Yes");
                }else{
                    flushText.setText("No");
                }
                //displays the sum of the faces of the cards the player has
                sumOfFaces.setText(Integer.toString(playerCards.sumOfFaces()));

                //Displays all the "Hearts" cards the player has
                if (playerCards.heartsList().isEmpty()){
                    cardsOfHearts.setText("No hearts");
                }else{
                    cardsOfHearts.setText(heartsListToString(playerCards.heartsList()));
                }

                //Displays "Yes" if player has a Queen of Spade, "No" otherwise
                if (playerCards.queenOfSpades() == true){
                    queenOfSpades.setText("Yes");
                }else{
                    queenOfSpades.setText("No");
                }
            }
        });
    }

    /**
     * When enter button is clicked the player's input is taken and displayed above the grey box
     * If the input is invalid, the player will get feedback in red about this in the grey box
     */
    private void enterAction(){
        enter.setOnAction(actionEvent -> {
            try{
                numOfCards = Integer.parseInt(input.getText());
                text.setText("No. of Cards: " + numOfCards);
                label.setText("Click Deal Hand to get cards \nwith the new amount");
                label.setTextFill(Color.BLACK);
            }catch (Exception error){
                label.setText("Enter a valid number \nbetween 1 and 52");
                label.setTextFill(Color.RED);
            }
        });
    }

    /**
     * returns a Hbox with all the buttons, a question about the number of cards and a input box
     */
    private HBox actionButtonsDisplay(){
        HBox buttons = new HBox();
        buttons.setPadding(new Insets(40,20,20,30));
        buttons.setSpacing(70);

        dealHand = new Button("Deal Hand");
        checkHand = new Button("Check Hand");
        enter = new Button("Enter");
        dealHand.setPrefSize(100,30);
        checkHand.setPrefSize(100,30);

        HBox inputDisplay = new HBox();
        inputDisplay.setSpacing(5);
        Text text = new Text("How many cards?");
        input.setMaxWidth(70);
        inputDisplay.getChildren().addAll(text,input, enter);

        buttons.getChildren().addAll(dealHand, checkHand, inputDisplay);

        return buttons;
    }

    /**
     * returns a Hbox with textfields used to display information and labels defining each textfield
     */
    private HBox informasjonDisplayBox(){
        HBox informasjon = new HBox();
        informasjon.setPadding(new Insets(10, 10, 10, 10));
        informasjon.setSpacing(20);

        VBox labels = new VBox();
        Label sum = new Label("Sum of faces: ");
        Label hearts = new Label("Cards of hearts: ");
        Label queenS = new Label("Queen of spades: ");
        Label flush = new Label("Flush: ");

        sum.setStyle("-fx-font-weight: bold");
        hearts.setStyle("-fx-font-weight: bold");
        queenS.setStyle("-fx-font-weight: bold");
        flush.setStyle("-fx-font-weight: bold");

        labels.setSpacing(30);
        labels.getChildren().addAll(sum, hearts, queenS, flush);

        VBox textArea = new VBox();
        textArea.setSpacing(20);
        textArea.getChildren().addAll(sumOfFaces,cardsOfHearts,queenOfSpades,flushText);


        informasjon.getChildren().addAll(labels, textArea);

        return informasjon;
    }

    /**
     * returns a grey AnchorPane with a label that will be used to display the player's cards
     */
    private AnchorPane displayBox(){
        AnchorPane box = new AnchorPane();
        box.setStyle("-fx-background-color: #d6d1d1");
        box.setPrefSize(350,300);

        label = new Label("Your cards will be displayed here");
        label.setStyle("-fx-font-weight: bold");
        label.setPadding(new Insets(20,10,10,70));
        label.setAlignment(Pos.CENTER);

        box.getChildren().add(label);

        return box;
    }

    /**
     * This is a help method used to display information about "Cards of hearts"
     * @param hearts a list that only contains cards of type hearts
     * @return text with cards of type hearts
     */
    private String heartsListToString(List<PlayingCard> hearts){
        String s = "";
        for (PlayingCard card: hearts) {
            s += card.getAsString() + ", ";
        }
        return s;
    }

    /**
     * A help method used to reset values in the textfields that show information about the cards
     */
    private void resetTextFields(){
        sumOfFaces.setText("");
        cardsOfHearts.setText("");
        queenOfSpades.setText("");
        flushText.setText("");
    }
}
