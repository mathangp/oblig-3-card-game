package ntnu.idatt2001.cardgame;

import java.util.*;
import java.util.Random;

public class DeckOfCards {

    private ArrayList<PlayingCard> cards;
    private final char[] suit = { 'S', 'H', 'D', 'C' }; //'S'=spade, 'H'=heart, 'D'=diamonds, 'C'=clubs

    /**
     * The constructor will make a list with 52 cards that will be
     * used to distribute cards to the players
     */
    public DeckOfCards(){
        cards = new ArrayList<>();
        //We make a complete deck of cards here
        for (char c : suit) {
            for (int j = 1; j <= 13; j++) {
                PlayingCard card = new PlayingCard(c, j);
                cards.add(card);
            }
        }
    }

    /**
     * The method checks for valid input, and will throw an IllegalArgumentException
     * if the number given is not between 1 and 52
     *
     * @param n number of cards that will be distributed to the player
     * @return collection: a list of randomly picked cards
     */
    public ArrayList<PlayingCard> dealHand(int n){
        if(n < 1 || n > 52){
            throw new IllegalArgumentException(n + " is not a valid number. \nPlease choose between 1 and 52");
        }
        ArrayList<PlayingCard> collection = new ArrayList<>();
        Random random = new Random();
        for (int i = 0; i < n; i++) {
            //generates a random number between 0 and 51.
            int randomNum = random.nextInt(cards.size());
            collection.add(cards.get(randomNum));
        }
        return collection;
    }

    /**
     * The method prints out all cards in the list in String form
     * @return a list of all the 52 cards in the deck
     */
    @Override
    public String toString() {
        String c = "[";
        for (PlayingCard card:cards) {
            c += card.getAsString() + ", ";
        }
        return c + "]";
    }
}
