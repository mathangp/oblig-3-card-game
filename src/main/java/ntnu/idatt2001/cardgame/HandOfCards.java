package ntnu.idatt2001.cardgame;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class HandOfCards {

    private ArrayList<PlayingCard> playerCards;
    private final DeckOfCards deck = new DeckOfCards();

    /**
     * This method will make a list of cards for the player by using the
     * dealHand method in the DeckOfHands class.
     *
     * @param numOfCards takes in the number of cards that the player will get
     */
    public HandOfCards(int numOfCards){
        playerCards = deck.dealHand(numOfCards);
    }

    /**
     * An empty list of cards is made
     */
    public HandOfCards(){
        playerCards = new ArrayList<>();
    }

    /**
     * The method checks for 5 cards flush
     * @return true if the player's list of cards has at least
     * 5 cards of type : hearts or spades or diamonds or clubs
     *          false otherwise.
     */
    public boolean checkFor5CardsFlush(){
        long hearts = playerCards.stream().filter(card -> card.getSuit() == 'H').count();
        long spades = playerCards.stream().filter(card -> card.getSuit() == 'S').count();
        long diamonds = playerCards.stream().filter(card -> card.getSuit() == 'D').count();
        long clubs = playerCards.stream().filter(card -> card.getSuit() == 'C').count();

        if (hearts >= 5 || spades >= 5 || diamonds >= 5 || clubs >= 5){
            return true;
        }
        return false;
    }

    /**
     *
     * @return the sum of all the card faces in the player's list of cards.
     */
    public int sumOfFaces(){
        return playerCards.stream().map(PlayingCard::getFace).reduce((a,b) -> a+b).get();
    }

    /**
     * This method plucks out all the cards that are of type hearts and returns it.
     * @return a list that contains only cards of type "hearts"
     */
    public List<PlayingCard> heartsList(){
        return playerCards.stream()
                .filter(card -> card.getSuit() == 'H')
                .collect(Collectors.toList());
    }

    /**
     *
     * @return true if the list has a card of type "spade" and the card is a queen = 12
     *          false if not.
     */
    public boolean queenOfSpades(){
        return playerCards.stream().anyMatch(card -> card.getSuit() == 'S' && card.getFace() == 12);
    }

    /**
     *
     * @return a list of cards that the player has
     */
    public ArrayList<PlayingCard> getPlayerCards() {
        return playerCards;
    }

    /**
     * The text is formatted so that it displays all the cards the player gets in a neat form
     * @return text with a list of cards the player has
     */
    @Override
    public String toString() {
        int cardsPerLine = 7;
        String c = "";
        for (int i = 1; i <= playerCards.size(); i++) {
            c += playerCards.get(i-1).getAsString() + ", ";
            if (i == cardsPerLine){
                c += "\n";
                cardsPerLine += 7;
            }
        }
        return "Your cards: \n [" + c + "]";
    }
}
