package ntnu.idatt2001.cardgame;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class DeckOfCardsTest {

    private DeckOfCards deckTest = new DeckOfCards();

    @Nested
    class DealHandMethodTest{

        //Positive test
        @Test
        public void dealHandValidNumber(){
            assertDoesNotThrow(()-> deckTest.dealHand(5));
        }

        //Negative test
        @Test
        public void dealHandZero(){
            assertThrows(IllegalArgumentException.class, ()-> deckTest.dealHand(0));
        }

        //Negative test
        @Test
        public void dealHandFiftyThree(){
            assertThrows(IllegalArgumentException.class, ()-> deckTest.dealHand(53));
        }

        //Negative test
        @Test void dealHandNegativeNumber(){
            assertThrows(IllegalArgumentException.class, ()-> deckTest.dealHand(-10));
        }
    }

}
