package ntnu.idatt2001.cardgame;

import org.junit.jupiter.api.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

public class HandOfCardsTest {

    //variables that will be used in the tests
    private HandOfCards cardsEight = new HandOfCards();
    private HandOfCards cardsTwo = new HandOfCards();

    /*
    data that needs to run before each test so that the
    cardsSeven and cardsTwo lists will have values in it to test
     */
    @BeforeEach
    public void runData(){
        cardsEight.getPlayerCards().add(new PlayingCard('H', 3));
        cardsEight.getPlayerCards().add(new PlayingCard('H', 6));
        cardsEight.getPlayerCards().add(new PlayingCard('S', 12));
        cardsEight.getPlayerCards().add(new PlayingCard('D', 1));
        cardsEight.getPlayerCards().add(new PlayingCard('S', 2));
        cardsEight.getPlayerCards().add(new PlayingCard('H', 9));
        cardsEight.getPlayerCards().add(new PlayingCard('H', 1));
        cardsEight.getPlayerCards().add(new PlayingCard('H', 10));

        cardsTwo.getPlayerCards().add(new PlayingCard('D', 1));
        cardsTwo.getPlayerCards().add(new PlayingCard('C', 2));
    }

    @Nested
    class CheckForFlushMethodTest{
        @Test
        @DisplayName("Check for flush with 8 cards in list")
        public void checkForFlushWithEightCards(){
            assertTrue(cardsEight.checkFor5CardsFlush());
        }

        @Test
        @DisplayName("Check for no flush with 2 cards in list")
        public void checkForNoFlushWithTwoCards(){
            assertFalse(cardsTwo.checkFor5CardsFlush());
        }
    }

    @Nested
    class SumOfFacesMethodTest{
        @Test
        public void sumOfEightCards(){
            assertEquals(44, cardsEight.sumOfFaces());
        }
        @Test
        public void sumOfTwoCards(){
            assertEquals(3, cardsTwo.sumOfFaces());
        }
    }

    @Nested
    class QueenOfSpadesMethodTest{
        @Test
        @DisplayName("Check for Queen of Spades in List with a Queen of Spade")
        public void queenOfSpadeWithEightCards(){
            assertTrue(cardsEight.queenOfSpades());
        }
        @Test
        @DisplayName("Check for Queen of Spades in List without a Queen of Spade")
        public void queenOfSpadeWithTwoCards(){
            assertFalse(cardsTwo.queenOfSpades());
        }
    }

    @Nested
    class HeartsListMethodTest{
        @Test
        @DisplayName("Check for list with hearts")
        public void heartsListTestWithEightCards(){
            List<PlayingCard> hearts = cardsEight.getPlayerCards().stream()
                    .filter(card -> card.getSuit() == 'H').collect(Collectors.toList());
            assertEquals(hearts, cardsEight.heartsList());
        }
        @Test
        @DisplayName("Check for empty hearts list")
        public void heartsListTestWithTwoCards(){
            List<PlayingCard> hearts = new ArrayList<>();
            assertEquals(hearts, cardsTwo.heartsList());
        }
    }
}